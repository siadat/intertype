diff = /usr/local/Cellar/diffutils/3.7/bin/diff

test:
	@go run ./intertype/ ./testfiles/... 2> /tmp/got || true
	@diff --color expected.txt /tmp/got

install:
	@#mv _go.mod go.mod
	@#mv _go.sum go.sum
	@go install ./intertype

	@mv go.mod _go.mod
	@mv go.sum _go.sum
	cd ~/clones/tools/gopls && go install .
	@mv _go.mod go.mod
	@mv _go.sum go.sum

diff:
	@vimdiff expected.txt /tmp/got

debug:
	@go run . ./testfiles/...
